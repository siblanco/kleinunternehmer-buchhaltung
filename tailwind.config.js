module.exports = {
	purge: false,
	theme: {
		fontFamily: {
			body: ['Open Sans', 'sans-serif'],
			display: ['Roboto', 'sans-serif'],
		},
		extend: {
			colors: {
				teal: {
					100: '#d7f1e9',
					200: '#aee3d2',
					300: '#86d4bc',
					400: '#5dc6a5',
					500: '#35b88f',
					600: '#2a9372',
					700: '#206e56',
					800: '#154a39',
					900: '#0b251d',
				},
			},
		},
	},
};
