import { app, BrowserWindow } from 'electron';
import * as path from 'path';
import isDev from 'electron-is-dev';

// disable security warnings since the app doesn't communicate with the dark world
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

let mainWindow: any;
const gotTheLock = app.requestSingleInstanceLock();
if (!gotTheLock) {
	app.quit();
} else {
	app.on('second-instance', () => {
		// Someone tried to run a second instance, we should focus our window.
		if (mainWindow) {
			if (mainWindow.isMinimized()) mainWindow.restore();
			mainWindow.focus();
		}
	});

	app.on('ready', createWindow);

	app.on('window-all-closed', () => {
		if (process.platform !== 'darwin') {
			app.quit();
		}
	});

	app.on('activate', () => {
		if (mainWindow === null) {
			createWindow();
		}
	});
}

function createWindow() {
	mainWindow = new BrowserWindow({
		width: 1200,
		height: 768,
		resizable: false,
		titleBarStyle: 'hiddenInset',
		webPreferences: {
			nodeIntegration: true,
			enableRemoteModule: true,
		},
	});

	let url;

	if (isDev) {
		// parces default port
		url = 'http://localhost:1234';
		mainWindow.webContents.openDevTools();
	} else {
		url = `file://${path.join(__dirname, './build/index.html')}`;
		mainWindow.removeMenu();
	}

	mainWindow.loadURL(url);
	mainWindow.on('closed', () => (mainWindow = null));
}
