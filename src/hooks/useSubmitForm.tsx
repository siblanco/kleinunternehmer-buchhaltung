import { useState, useEffect } from 'react';
import { SubmitHandler } from 'react-hook-form';

// useSubmitForm = <T extends unknown>(data: T) => {
//
// }

function useSubmitForm<FormValues>(
	submitForm: (data: any) => boolean | Promise<any>,
	errorMsg: string
): { error: string; onSubmit: SubmitHandler<FormValues>; success: boolean } {
	const [error, setError] = useState('');
	const [success, setSuccess] = useState(false);

	let hideMessage = 0;

	useEffect(() => {
		const clearTimer = () => {
			if (hideMessage) {
				window.clearTimeout(hideMessage);
			}
		};
		return clearTimer;
	}, [hideMessage]);

	const onSubmit: SubmitHandler<FormValues> = (data) => {
		const formSuccess = submitForm(data);
		if (!formSuccess) {
			setError(errorMsg);
			setSuccess(false);
		} else {
			setError('');
			setSuccess(true);
			hideMessage = window.setTimeout(() => {
				setSuccess(false);
			}, 3000);
		}
	};

	return { error, onSubmit, success };
}

export default useSubmitForm;
