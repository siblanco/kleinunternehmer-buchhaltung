export interface IUser {
	vorname: string;
	nachname: string;
	company: string;
	taxno: string;
	street: string;
	plz: string;
	city: string;
	tel: string;
	iban: string;
	bic: string;
	web: string;
	email: string;
	logo: string;
	kleinunternehmer: string;
	invoiceType: 'yearly' | 'monthly';
}

export const InvoiceTypeOptions = [
	{
		name: 'Jährlich fotlaufende Rechnungsnummer',
		value: 'yearly',
	},
	{
		name: 'Monatlich fotlaufende Rechnungsnummer',
		value: 'monthly',
	},
];
