export interface ICustomer {
	name: string;
	company: string;
	address: string;
	city: string;
	postalcode: string;
	customern: string;
	uid?: string;
	country?: string;
}
