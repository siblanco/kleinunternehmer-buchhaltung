export interface IRechnungsPosition {
	id: number;
	desc: string;
	price: number | string;
	currency: TCurrencies;
}

export interface IInvoice {
	invoicen: string;
	customern: string;
	positions: string;
	total: number;
	date: string;
	termOfPayment: number;
	currency: TCurrencies;
	performancePeriod?: string;
}

export interface IExpense {
	desc: string;
	date: string;
	invoicen: string;
	price: number;
	pdf: File;
	currency: TCurrencies;
}

export type TCurrencies = 'euro' | 'chf';

export enum ECurrencyTax {
	EURO = 'Kein Steuerausweis aufgrund der Anwendung der Kleinunternehmerregelung (§ 19 UStG)',
	CHF = 'Nicht steuerbare sonstige Leistung nach § 3a Abs. 2 UStG. Leistungsempfänger schuldet die Umsatzsteuer.',
}

export enum ECurrencySign {
	EURO = '€',
	CHF = 'CHF',
}
