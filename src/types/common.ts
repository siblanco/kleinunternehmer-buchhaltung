export interface IAppColors {
	[key: string]: string;
}
