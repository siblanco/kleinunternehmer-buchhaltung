export interface IFormField {
	label: string;
	name: string;
	checked?: boolean;
	extraHandler?: (value: string) => void;
	placeholder?: string;
	value?: string;
	disabled?: boolean;
	readOnly?: boolean;
	width?: string;
	type?: string;
	hidden?: boolean;
	required?: boolean | string;
}

export interface ISelect extends IFormField {
	options: {
		value: string | number;
		name: string | number;
	}[];
}

export interface ITextArea extends IFormField {
	rows?: number;
}
