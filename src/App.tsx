import React, { useState } from 'react';
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import Kunden from './views/Kunden';
import Rechnungen from './views/Rechnungen';
import Setup from './views/Setup';
import EditUser from './views/Setup/EditUser';
import About from './views/About';

import Navigation from './components/layout/Navigation';
import Layout from './components/layout/Layout';
import { User } from './node/user';
import { IUser } from './types';

export const App = () => {
	const [user, setUser] = useState<IUser>(User.getUserData());

	if (!user?.email) {
		return (
			<Layout center>
				<Setup updateUser={() => setUser({ ...User.getUserData() })} />
			</Layout>
		);
	}

	return (
		<Layout>
			<Router>
				<Navigation />
				<main>
					<Switch>
						<Route path="/" exact>
							<Redirect to="/kunden" />
						</Route>
						<Route path="/kunden" component={Kunden} />
						<Route path="/rechnungen" component={Rechnungen} />
						<Route path="/setup" component={EditUser} />
						<Route path="/help" component={About} />
					</Switch>
				</main>
			</Router>
		</Layout>
	);
};
