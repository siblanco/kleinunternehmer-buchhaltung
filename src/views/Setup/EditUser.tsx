import React from 'react';
import Headline from '../../components/common/Headline';
import Form from '../../components/forms/Form';
import Input from '../../components/forms/Input';
import Checkbox from '../../components/forms/Checkbox';
import Text from '../../components/common/Text';
import { IUser, InvoiceTypeOptions } from '../../types';
import { User } from '../../node/user';
import useSubmitForm from '../../hooks/useSubmitForm';
import HandleUpload from '../../components/forms/HandleUpload';
import Label from '../../components/forms/Label';
import Select from '../../components/forms/Select';

const EditUser = () => {
	const user = User.getUserData();

	const submitForm = (data: IUser) => {
		return User.updateUserData(data);
	};

	const { error, onSubmit, success } = useSubmitForm<IUser>(submitForm, 'Oops!');

	return (
		<div className="pr-12 mx-auto">
			<Headline tag="h1">Hier kannst du deine Daten bearbeiten</Headline>
			<Text marginBottom>
				<p>
					Diese Daten werden zur Rechnungserstellung genutzt. Du kannst Sie später auch
					nochmal anpassen.
				</p>
			</Text>
			<Form
				errorMsg={error}
				submitSuccess={success}
				onSubmit={onSubmit}
				submitText="Daten speichern"
				successMsg="Daten erfolgreich gespeichert."
			>
				<Input value={user.vorname} label="Vorname" name="vorname" required width="w-1/2" />
				<Input
					value={user.nachname}
					label="Nachname"
					name="nachname"
					required
					width="w-1/2"
				/>
				<Input value={user.company} label="Firma" name="company" required width="w-1/2" />
				<Input
					value={user.taxno}
					label="Steuernummer"
					name="taxno"
					required
					width="w-1/2"
				/>
				<Input
					value={user.street}
					label="Straße mit Nummer"
					name="street"
					required
					width="w-1/3"
				/>
				<Input value={user.plz} label="PLZ" name="plz" required width="w-1/3" />
				<Input value={user.city} label="Ort" name="city" required width="w-1/3" />
				<Input value={user.tel} label="Telefonnummer" name="tel" required width="w-1/3" />
				<Input
					value={user.email}
					label="E-Mail-Adresse"
					name="email"
					required
					width="w-1/3"
				/>
				<Input value={user.web} label="Internetadresse" name="web" width="w-1/3" />
				<Input value={user.iban} label="IBAN" name="iban" required width="w-1/2" />
				<Input value={user.bic} label="BIC" name="bic" required width="w-1/2" />

				<Select
					value={user.invoiceType}
					label="Rechnungsnummer"
					name="invoiceType"
					options={InvoiceTypeOptions}
				></Select>

				<div className="mb-2" data-name="logo-preview">
					<Label label="Aktuelles Logo" />
					<img src={user.logo} alt="Logo" className="w-64 block mt-2" />
				</div>

				<HandleUpload
					formSent={success}
					label="Logo ändern (jpg, jpeg, png)"
					value="logo"
					extensions={['jpg', 'jpeg', 'png']}
					type="image"
					data-name="logo-uploader"
				/>

				<Input label="Logo" name="logo" value={user.logo} disabled hidden />

				<Checkbox disabled label="Kleinunternehmerregelung" name="kleinunternehmer" />
			</Form>
		</div>
	);
};

export default EditUser;
