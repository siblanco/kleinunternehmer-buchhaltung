import React from 'react';

import { Files } from '../../node/files';

import Button from '../../components/common/Button';
import Headline from '../../components/common/Headline';
import Text from '../../components/common/Text';

interface ISetupIntroProps {
	setMode: (mode: string) => void;
}

const Intro = ({ setMode }: ISetupIntroProps) => {
	return (
		<div className="w-full px-32 mx-auto">
			<Headline tag="h1">Willkommen in der Buchhaltungsapp für Kleinunternehmer.</Headline>
			<Text marginBottom>
				<p>
					Diese App hilft dir beim Erstellen deiner Dokumente für die jährliche
					Steuererklärung.
				</p>
				<p>
					In deinem Nutzerverzeichnis werden Rechnungen sortiert nach Einnahmen, Ausgaben,
					Monat und Jahr abgelegt. Passend dazu werden Einträge in eine
					Einnahmeüberschussrechnung vorgenommen. Deine Daten werden in{' '}
					<b>{Files.getHomePath()} </b>
					abgelegt. Diese solltest du regelmäßig sichern.
				</p>
				<p>Fangen wir mit der Anlage deiner Daten an.</p>
			</Text>

			<Button handleClick={() => setMode('create')}>Zum Formular</Button>
		</div>
	);
};

export default Intro;
