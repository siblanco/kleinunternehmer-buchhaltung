import React, { useState } from 'react';

import CreateUser from './CreateUser';
import Intro from './Intro';

export interface ISetupUpdateUserProps {
	updateUser: () => void;
}

const Setup = ({ updateUser }: ISetupUpdateUserProps) => {
	const [mode, setMode] = useState('intro');

	if (mode === 'create') return <CreateUser updateUser={updateUser} />;
	return <Intro setMode={setMode} />;
};

export default Setup;
