import React from 'react';

import Form from '../../components/forms/Form';
import Input from '../../components/forms/Input';
import Checkbox from '../../components/forms/Checkbox';
import Headline from '../../components/common/Headline';
import Text from '../../components/common/Text';
import useSubmitForm from '../../hooks/useSubmitForm';

import { IUser, InvoiceTypeOptions } from '../../types';
import { User } from '../../node/user';
import { ISetupUpdateUserProps } from '.';
import HandleUpload from '../../components/forms/HandleUpload';
import Select from '../../components/forms/Select';

const CreateUser = ({ updateUser }: ISetupUpdateUserProps) => {
	const submitForm = (data: IUser) => {
		return User.updateUserData(data);
	};

	const { error, onSubmit, success } = useSubmitForm<IUser>(submitForm, 'Oops!');

	return (
		<div className="w-full pr-12 mx-auto">
			<Headline tag="h1">Fülle deine Daten aus.</Headline>
			<Text marginBottom>
				<p>
					Diese Daten werden zur Rechnungserstellung genutzt. Du kannst Sie später auch
					nochmal anpassen.
				</p>
			</Text>
			<Form
				errorMsg={error}
				submitSuccess={success}
				successAction={updateUser}
				onSubmit={onSubmit}
				submitText="Daten speichern"
				successMsg="Daten erfolgreich gespeichert."
			>
				<Input label="Vorname" name="vorname" required width="w-1/2" />
				<Input label="Nachname" name="nachname" required width="w-1/2" />
				<Input label="Firma" name="company" required width="w-1/2" />
				<Input label="Steuernummer" name="taxno" required width="w-1/2" />
				<Input label="Straße mit Nummer" name="street" required width="w-1/3" />
				<Input label="PLZ" name="plz" required width="w-1/3" />
				<Input label="Ort" name="city" required width="w-1/3" />
				<Input label="Telefonnummer" name="tel" required width="w-1/3" />
				<Input label="E-Mail-Adresse" name="email" required width="w-1/3" />
				<Input label="Internetadresse" name="web" width="w-1/3" />
				<Input label="IBAN" name="iban" required width="w-1/2" />
				<Input label="BIC" name="bic" required width="w-1/2" />

				<Select
					label="Rechnungsnummer"
					name="invoiceType"
					options={InvoiceTypeOptions}
				></Select>

				<HandleUpload
					formSent={success}
					label="Logo hochladen (jpg, jpeg, png)"
					value="logo"
					extensions={['jpg', 'jpeg', 'png']}
					type="image"
					data-name="logo-uploader"
				/>

				<Input
					label="Logo"
					name="logo"
					disabled
					required="Bitte lade ein Logo hoch"
					hidden
				/>

				<Checkbox disabled label="Kleinunternehmerregelung" name="kleinunternehmer" />
			</Form>
		</div>
	);
};

export default CreateUser;
