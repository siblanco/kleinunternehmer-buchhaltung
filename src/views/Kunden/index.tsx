import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';

import Headline from '../../components/common/Headline';

import CreateCustomer from './CreateCustomer';
import EditCustomer from './EditCustomer';
import CustomerOverview from './CustomerOverview';

const Kunden = () => {
	const { path } = useRouteMatch();
	return (
		<div>
			<Headline tag="h1">Kunden</Headline>
			<Switch>
				<Route exact path={path} component={CustomerOverview} />
				<Route path={`${path}/create`} component={CreateCustomer} />
				<Route path={`${path}/edit/:customern`} component={EditCustomer} />
			</Switch>
		</div>
	);
};

export default Kunden;
