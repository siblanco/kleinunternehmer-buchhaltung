import React from 'react';
import { useParams, Link } from 'react-router-dom';

import { Customers } from '../../node/customers';
import useSubmitForm from '../../hooks/useSubmitForm';

import { ICustomer } from '../../types';

import Headline from '../../components/common/Headline';
import Form from '../../components/forms/Form';
import Input from '../../components/forms/Input';

const EditCustomer = () => {
	const { customern } = useParams<any>();
	const customer = Customers.getCustomer(customern);

	const { error, onSubmit, success } = useSubmitForm<ICustomer>(
		Customers.editCustomer,
		'Kundennummer existiert bereits.'
	);

	return (
		<div>
			<Headline tag="h2">{customer.name} bearbeiten</Headline>
			<Form
				onSubmit={onSubmit}
				submitSuccess={success}
				successMsg="Kunde erfolgreich bearbeitet!"
				errorMsg={error}
			>
				<Input value={customer.name} label="Name*" name="name" required width="w-1/2" />
				<Input
					value={customer.company}
					label="Firma*"
					name="company"
					required
					width="w-1/2"
				/>
				<Input
					value={customer.address}
					label="Straße mit Nummer*"
					name="address"
					required
					width="w-1/2"
				/>
				<Input
					value={customer.postalcode}
					label="PLZ*"
					name="postalcode"
					required
					width="w-1/2"
				/>
				<Input value={customer.city} label="Ort*" name="city" width="w-1/2" />

				<Input label="Land" name="country" value={customer.country} width="w-1/2" />

				<Input
					value={customer.customern}
					label="Kundennummer*"
					name="customern"
					readOnly
					required
					width="w-1/2"
				/>

				<Input label="UID" name="uid" value={customer.uid} width="w-1/2" />
			</Form>
			<div className="w-full text-right">
				<Link to="/kunden">
					<button className="font-display">Zurück</button>
				</Link>
			</div>
		</div>
	);
};

export default EditCustomer;
