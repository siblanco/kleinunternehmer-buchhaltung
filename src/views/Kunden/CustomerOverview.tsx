import React, { useEffect, useState } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { FaTrash, FaPencilAlt } from 'react-icons/fa';

import { Customers } from '../../node/customers';

import Headline from '../../components/common/Headline';
import { ICustomer } from '../../types';
import { IoIosPersonAdd } from 'react-icons/io';

const CustomerOverview = () => {
	const { url } = useRouteMatch();
	const [allCustomers, setAllCustomers] = useState<ICustomer[]>([]);

	useEffect(() => {
		setAllCustomers(Customers.getAllCustomers());
	}, []);

	const deleteCustomer = (customern: string) => {
		if (window.confirm('Kunde löschen?')) {
			Customers.deleteCustomer(customern);
			setAllCustomers(allCustomers.filter((customer) => customer.customern !== customern));
		}
	};

	return (
		<div>
			<div className="flex flex-wrap mb-4">
				{allCustomers.map((customer) => (
					<div
						key={customer.customern}
						className="text-md p-6 bg-white text-gray-900 shadow-md rounded-md mr-6 mb-6 relative w-1/4"
					>
						<Headline tag="h4">{customer.customern}</Headline>
						<p className="font-display">{customer.company}</p>
						<p>{customer.name}</p>
						<p>
							{customer.address} <br />
							{customer.postalcode} {customer.city}
						</p>
						<FaTrash
							title="Kunde löschen"
							className="absolute top-0 m-2 right-0 w-4 h-4 cursor-pointer text-red-500"
							onClick={() => deleteCustomer(customer.customern)}
						/>
						<Link to={`${url}/edit/${customer.customern}`}>
							<FaPencilAlt
								title="Kunde bearbeiten"
								className="absolute top-0 m-2 mr-8 right-0 w-4 h-4 cursor-pointer text-teal-500"
							/>
						</Link>
					</div>
				))}
				<div className="flex items-center justify-center p-6 mb-6">
					<Link to={`${url}/create`}>
						<IoIosPersonAdd
							className="h-12 w-12 cursor-pointer hover:translate-x-1 transform transition-transform duration-150 text-teal-500"
							title="Neuen Kunden hinzufügen"
						/>
					</Link>
				</div>
			</div>
		</div>
	);
};

export default CustomerOverview;
