import React from 'react';
import { Link } from 'react-router-dom';

import { Customers } from '../../node/customers';
import { ICustomer } from '../../types';

import useSubmitForm from '../../hooks/useSubmitForm';
import Headline from '../../components/common/Headline';
import Form from '../../components/forms/Form';
import Input from '../../components/forms/Input';

const CreateCustomer = () => {
	const { error, onSubmit, success } = useSubmitForm<ICustomer>(
		Customers.createCustomer,
		'Kundennummer existiert bereits.'
	);

	return (
		<div>
			<Headline tag="h2">Neuen Kunden erstellen</Headline>
			<Form
				onSubmit={onSubmit}
				submitSuccess={success}
				successMsg="Kunde erfolgreich erstellt"
				errorMsg={error}
			>
				<Input label="Name*" name="name" required width="w-1/2" />
				<Input label="Firma*" name="company" required width="w-1/2" />
				<Input label="Straße mit Nummer*" name="address" required width="w-1/2" />
				<Input label="PLZ*" name="postalcode" required width="w-1/2" />
				<Input label="Ort*" name="city" required width="w-1/2" />
				<Input label="Land" name="country" required width="w-1/2" />
				<Input
					label="Kundennummer*"
					name="customern"
					value={Math.floor(Math.random() * Math.pow(10, 12)).toString()}
					required
					width="w-1/2"
				/>
				<Input label="UID" name="uid" width="w-1/2" />
			</Form>
			<div className="w-full text-right">
				<Link to="/kunden">
					<button className="font-display">Zurück</button>
				</Link>
			</div>
		</div>
	);
};

export default CreateCustomer;
