import React from 'react';
import Form from '../../components/forms/Form';
import Input from '../../components/forms/Input';
import useSubmitForm from '../../hooks/useSubmitForm';
import { Files } from '../../node/files';
import { Invoices } from '../../node/invoices';

type TRewriteInvoiceFormValues = {
	invoicen: string;
};

const rewriteInvoice = async ({ invoicen }: TRewriteInvoiceFormValues) => {
	try {
		Invoices.rewriteExistingInvoice(invoicen);
	} catch {
		throw new Error('Something went terribly wrong mate.');
	}
};

const RewriteInvoice = () => {
	const { error, onSubmit, success } = useSubmitForm<TRewriteInvoiceFormValues>(
		rewriteInvoice,
		'Oops ein Fehler!'
	);

	return (
		<Form
			errorMsg={error}
			onSubmit={onSubmit}
			submitSuccess={success}
			successMsg="Rechnung erfolgreich neu geschrieben."
			submitText="Erstellen"
		>
			<div data-name="description">
				<p>Die Rechnung wird im Verzeichnis {Files.getHomePath()} gespeichert.</p>
			</div>

			<Input name="invoicen" label="Rechnungsnummer" required />
			<div data-name="placeholder"></div>
		</Form>
	);
};

export default RewriteInvoice;
