import React from 'react';

import Form from '../../components/forms/Form';
import Select from '../../components/forms/Select';
import Input from '../../components/forms/Input';
import Positions from './Positions';

import useSubmitForm from '../../hooks/useSubmitForm';

import { Invoices } from '../../node/invoices';
import { Customers } from '../../node/customers';

import { IRechnungsPosition, TCurrencies } from '../../types';
import Headline from '../../components/common/Headline';
import { CurrencySelect } from '../../components/forms/Currency/currency-select';

interface IRechnungsValues {
	customern: string;
	positions: IRechnungsPosition[];
	date: string;
	currency: TCurrencies;
	performancePeriod?: string;
	termOfPayment: number;
	valueTaxInfo: string;
}

const AddIncome = () => {
	const { error, onSubmit, success } = useSubmitForm<IRechnungsValues>(
		Invoices.createInvoice,
		'Oops! Es ist ein Fehler aufgetreten. Bitte überprüfe deine Angaben.'
	);

	const customers = Customers.getAllCustomers();

	const customerSelectOptions = customers.map((customer) => ({
		name: `${customer.name} ${customer.company ? '| ' + customer.company : ''}`,
		value: customer.customern,
	}));

	if (!customers.length) {
		return <Headline tag="h3">Lege Kunden an um Rechnungen zu erstellen.</Headline>;
	}

	return (
		<div>
			<Form
				errorMsg={error}
				onSubmit={onSubmit}
				submitSuccess={success}
				successMsg="Rechnung erfolgreich erstellt"
				submitText="Rechnung erstellen"
			>
				<Select
					required
					label="Kunde"
					name="customern"
					options={customerSelectOptions}
					width="w-1/2"
				/>

				<Input
					required="Bitte gebe ein Rechnungsdatum an"
					type="date"
					label="Rechnungsdatum"
					name="date"
					width="w-1/2"
				/>

				<CurrencySelect data-name="currency-select" width="w-1/2" />

				<Input
					label="Zahlungsziel in Tagen"
					name="termOfPayment"
					required="Bitte gebe das Zahlungsziel in Tagen an"
					width="w-1/2"
					type="number"
					value="14"
				/>

				<Input
					label="Leistungszeitraum"
					name="performancePeriod"
					value="Das Rechnungsdatum entspricht dem Lieferdatum."
					required="Bitte gebe den Leistungszeitraum an"
				/>

				<Positions cleanUp={success} data-name="position-adder" />

				<Input
					required="Bitte gebe Rechnungspositionen an."
					label="Positionen"
					name="positions"
					hidden
				/>
			</Form>
		</div>
	);
};

export default AddIncome;
