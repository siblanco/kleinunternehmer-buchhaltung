import React from 'react';

import { Invoices } from '../../node/invoices';
import * as currencyFormatter from '../../node/currency';

import Form from '../../components/forms/Form';
import useSubmitForm from '../../hooks/useSubmitForm';
import Input from '../../components/forms/Input';
import HandleUpload from '../../components/forms/HandleUpload';

import { IExpense } from '../../types';
import CurrencyLabel from '../../components/forms/Currency';
import { CurrencySelect } from '../../components/forms/Currency/currency-select';

const AddExpense = () => {
	const { error, onSubmit, success } = useSubmitForm<IExpense>(submitExpense, 'Oops ein Fehler!');

	function submitExpense(data: IExpense) {
		// expenses will always be in euro for me
		const euroPrice = currencyFormatter.euro(data.price).format();
		data.price = euroPrice;
		return Invoices.createExpense(data);
	}

	return (
		<Form
			errorMsg={error}
			onSubmit={onSubmit}
			submitSuccess={success}
			successMsg="Ausgabe erfolgreich eingetragen."
		>
			<CurrencySelect width="w-full" />
			<Input name="desc" label="Beschreibung" width="w-1/2" required />

			<CurrencyLabel reset={success} label="Betrag" name="price" data-width="w-1/2" />

			<Input type="date" name="date" label="Datum" width="w-1/2" required />
			<Input name="invoicen" label="Rechnungsnummer" width="w-1/2" required />

			<Input
				required="Bitte lade ein Dokument hoch, welches deine Ausgabe quittiert."
				hidden
				name="pdf"
				label="Dokument"
			/>

			<HandleUpload
				formSent={success}
				label="Dokument hochladen (PDF)"
				extensions={['pdf']}
				value="pdf"
				data-name="document-uploader"
			/>
		</Form>
	);
};

export default AddExpense;
