import React, { useState, useEffect } from 'react';
import { useFormContext } from 'react-hook-form';
import { FaTrash } from 'react-icons/fa';

import Label from '../../components/forms/Label';
import Headline from '../../components/common/Headline';

import * as currencyFormatter from '../../node/currency';
import { IRechnungsPosition } from '../../types';

import { veryRandomID } from '../../utils';

interface IPositionsProps {
	cleanUp: boolean;
}

const Positions = ({ cleanUp }: IPositionsProps) => {
	const [positions, setPositions] = useState<IRechnungsPosition[]>([]);
	const [desc, setDesc] = useState('');
	const [price, setPrice] = useState('');
	const [error, setError] = useState(false);

	const { setValue, watch } = useFormContext();
	const currency = watch('currency', 'euro');

	useEffect(() => {
		if (cleanUp) {
			setPositions([]);
		}
	}, [cleanUp]);

	const addPosition = () => {
		if (!desc || !price) return setError(true);
		setError(false);

		const totalPositions: IRechnungsPosition[] = [
			...positions,
			{
				desc,
				price: (currencyFormatter as any)[currency](price).format(),
				id: veryRandomID(),
				currency: currency,
			},
		];

		setPositions(totalPositions);
		setValue('positions', JSON.stringify(totalPositions), { shouldDirty: true });

		setDesc('');
		setPrice('');
	};

	const deletePosition = (id: number) => {
		setPositions(positions.filter((pos) => pos.id !== id));
	};

	const handleEnter = (e: React.KeyboardEvent) => {
		if (e.key === 'Enter') {
			addPosition();
		}
	};

	return (
		<div className="flex space-x-6">
			<div className="w-1/2">
				<Label label="Position hinzufügen" />
				<input
					className="input-field mb-2"
					placeholder="Beschreibung"
					onChange={({ target }) => setDesc(target.value)}
					onKeyDown={(e) => handleEnter(e)}
					value={desc}
				/>

				<input
					className="input-field"
					type="number"
					placeholder="Betrag"
					value={price}
					onChange={({ target }) => setPrice(target.value)}
					onKeyDown={(e) => handleEnter(e)}
				/>
				<div className="mb-4 pl-4 pt-1 font-display text-xs">
					{price && (currencyFormatter as any)[currency](price).format()}
				</div>

				{error && (
					<p className="text-red-500 uppercase text-xs font-display mb-4">
						Gebe eine Beschreibung und einen Betrag ein.
					</p>
				)}
				<button
					type="button"
					className="bg-gray-700 text-gray-100 py-1 px-4 font-display rounded-sm shadow-sm"
					onClick={addPosition}
				>
					Hinzufügen
				</button>
			</div>

			<div className="w-1/2">
				<Headline tag="h4">Positionen</Headline>
				<ul>
					{positions.map((pos) => (
						<li
							key={pos.id}
							className="flex items-center justify-between mt-2 py-2 px-4 bg-white font-display shadow-sm rounded-sm"
						>
							<span className="block mr-8">{pos.desc}</span>
							<span className="block ml-auto mr-6">{pos.price}</span>

							<FaTrash
								className="text-red-500 cursor-pointer flex-shrink-0"
								title="Position löschen"
								onClick={() => deletePosition(pos.id)}
							/>
						</li>
					))}
				</ul>
			</div>
		</div>
	);
};

export default Positions;
