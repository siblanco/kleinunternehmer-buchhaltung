import React from 'react';
import { Switch, useRouteMatch, Route, NavLink } from 'react-router-dom';
import AddIncome from './AddIncome';
import AddExpense from './AddExpense';
import Headline from '../../components/common/Headline';
import RewriteInvoice from './RewriteInvoice';

const Rechnungen = () => {
	const { path, url } = useRouteMatch();

	return (
		<div>
			<Headline tag="h1">Rechnungen</Headline>
			<div className="my-8 space-x-6">
				<NavLink
					className="bg-white rounded-sm border border-transparent text-gray-900 py-2 px-6 font-display transition-all duration-150"
					activeClassName="text-teal-500 border-teal-500"
					exact
					to={`${url}`}
				>
					Einnahmen eintragen
				</NavLink>
				<NavLink
					className="bg-white rounded-sm border border-transparent text-gray-900 py-2 px-6 font-display transition-all duration-150"
					activeClassName="text-teal-500 border-teal-500"
					to={`${url}/ausgabe`}
				>
					Ausgaben eintragen
				</NavLink>
				<NavLink
					className="bg-white rounded-sm border border-transparent text-gray-900 py-2 px-6 font-display transition-all duration-150"
					activeClassName="text-teal-500 border-teal-500"
					to={`${url}/rewrite`}
				>
					Rechnung neu schreiben
				</NavLink>
			</div>
			<Switch>
				<Route path={path} exact component={AddIncome} />
				<Route path={`${path}/ausgabe`} component={AddExpense} />
				<Route path={`${path}/rewrite`} component={RewriteInvoice} />
			</Switch>
		</div>
	);
};

export default Rechnungen;
