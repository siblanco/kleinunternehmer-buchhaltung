import React, { useState } from 'react';
const { remote } = window.require('electron');
const { join } = window.require('path');

import { Files } from '../node/files';
import { zipDirectory } from '../node/archiver';

import Headline from '../components/common/Headline';
import Text from '../components/common/Text';
import Button from '../components/common/Button';

import { openLinkInDefaultBrowser } from '../utils';

const Status = ({ msg, error = false }: { msg: string; error?: boolean }) => (
	<p className={`font-display ${error ? 'text-read-500' : 'text-teal-700'}`}>{msg}</p>
);

const About = () => {
	const [backup, setBackup] = useState(false);
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState<any>();

	async function createBackup() {
		setLoading(true);
		setBackup(false);
		setError(false);

		const dir = await remote.dialog.showOpenDialog({
			properties: ['openDirectory'],
		});
		const destDir = dir.filePaths[0];

		if (!destDir) {
			setLoading(false);
			return;
		}

		const date = new Date();
		const zipped = await zipDirectory(
			Files.homedir,
			join(destDir, `Buchhaltung-${date.getTime()}.zip`)
		);

		if (!zipped) {
			setError(zipped);
		}

		setLoading(false);
		setError(false);
		setBackup(true);
	}

	return (
		<div>
			<Headline tag="h1">Buchhaltungsapp für Kleinunternehmer</Headline>
			<Text marginBottom>
				<p>
					Diese App hilft dir beim Erstellen deiner Dokumente für die jährliche
					Steuererklärung.
				</p>
				<p>
					In deinem Nutzerverzeichnis werden Rechnungen sortiert nach Einnahmen, Ausgaben,
					Monat und Jahr abgelegt. Passend dazu werden Einträge in eine
					Einnahmeüberschussrechnung vorgenommen.
				</p>
				<div>
					<p className="mb-4">
						Deine Daten werden in <b>{Files.getHomePath()}</b> abgelegt. Mit einem
						Backup kannst du diese Software auf jedem Rechner einfach weiter benutzen.
						Ersetze dazu einfach den kompletten Inhalt von <b>{Files.getHomePath()}</b>{' '}
						mit deinem entpackten Backup.
					</p>
					<div className="space-x-4">
						<Button handleClick={createBackup}>Backup erstellen</Button>
					</div>
					<div className="mt-2 pl-4">
						{loading && <Status msg="Lade..." />}
						{backup && <Status msg="Backup erstellt. Backups bitte stets prüfen!" />}
						{error && (
							<Status
								msg={`Fehler! Bitte Hassan kontaktieren: ${JSON.stringify(error)}`}
								error
							/>
						)}
					</div>
				</div>
			</Text>
			<Text>
				<p>
					Feedback, Anregungen und Wünsche kannst du mir gerne per Mail zukommen lassen:
					<br />
					<a
						onClick={(e) => openLinkInDefaultBrowser(e)}
						href="mailto:hallo@siblanco.dev?subject=Hi Hassan"
						title="Schreib mir ne Mail!"
						className="font-display text-teal-700"
					>
						hallo@siblanco.dev
					</a>
				</p>
			</Text>
		</div>
	);
};

export default About;
