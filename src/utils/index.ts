export const veryRandomID = () => {
	return Math.floor(Math.random() * Date.now());
};

export const generateGermanDate = (date: string) => {
	const d = new Date(date);
	const ye = new Intl.DateTimeFormat('de-DE', { year: 'numeric' }).format(d);
	const mo = new Intl.DateTimeFormat('de-DE', { month: '2-digit' }).format(d);
	const da = new Intl.DateTimeFormat('de-DE', { day: '2-digit' }).format(d);
	return `${da}.${mo}.${ye}`;
};

export const openLinkInDefaultBrowser = (
	event: React.MouseEvent<HTMLAnchorElement, MouseEvent>
) => {
	event.preventDefault();
	const link = (<HTMLAnchorElement>event.target).href;
	window.require('electron').shell.openExternal(link);
};
