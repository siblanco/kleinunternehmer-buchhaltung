import React from 'react';

interface IHeadlineProps {
	tag: 'h1' | 'h2' | 'h3' | 'h4';
	children: React.ReactNode;
}

type THeadlineStyles = {
	h1: string;
	h2: string;
	h3: string;
	h4: string;
	[key: string]: string;
};

const Headline = ({ children, tag }: IHeadlineProps) => {
	const headlineStyles: THeadlineStyles = {
		h1: 'text-4xl mb-6',
		h2: 'text-3xl mb-4',
		h3: 'text-2xl mb-2',
		h4: 'text-xl',
	};

	const Wrapper = tag;
	return <Wrapper className={headlineStyles[tag]}>{children}</Wrapper>;
};

export default Headline;
