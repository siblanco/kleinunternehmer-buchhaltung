import React from 'react';

import { IAppColors } from '../../../types';

interface IButton extends React.ButtonHTMLAttributes<HTMLButtonElement> {
	type?: 'button' | 'submit';
	disabled?: boolean;
	color?: string;
	handleClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
	children: React.ReactNode;
}

const Button = ({
	type = 'button',
	color = 'white',
	disabled = false,
	handleClick,
	children,
}: IButton) => {
	const colors: IAppColors = {
		white: 'bg-white hover:shadow-md text-gray-900',
		danger: 'bg-red-900 text-white hover:bg-red-600',
	};

	return (
		<button
			className={`
                transition-all font-display inline-block py-1 px-4 rounded cursor-pointer duration-150 focus:outline-none 
                ${colors[color]} 
                ${disabled ? 'opacity-50 pointer-events-none' : null}`}
			type={type}
			{...(handleClick && { onClick: handleClick })}
		>
			{children}
		</button>
	);
};

export default Button;
