import React from 'react';

interface ITextProps {
	children: React.ReactNode;
	size?: 'big' | 'medium' | 'small';
	marginBottom?: boolean;
}

type TTextStyles = {
	big: string;
	medium: string;
	small: string;
	[key: string]: string;
};

const Text = ({ children, size, marginBottom }: ITextProps) => {
	const TextStyles: TTextStyles = {
		big: 'text-xl',
		medium: 'text-lg',
		small: 'text-md',
	};

	if (!size) size = 'medium';

	return (
		<div className={`${TextStyles[size]} ${marginBottom ? 'mb-8' : ''} space-y-4`}>
			{children}
		</div>
	);
};

export default Text;
