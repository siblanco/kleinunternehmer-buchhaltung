import React from 'react';
import { openLinkInDefaultBrowser } from '../../../utils';

interface ILayoutProps {
	children: React.ReactNode;
	center?: boolean;
}

const Layout = ({ children, center = false }: ILayoutProps) => (
	<div
		className={`min-h-screen bg-teal-100 text-gray-700 p-12 relative 
                ${center ? 'flex items-center justify-center' : ''}`}
	>
		{children}

		<div className="copyright absolute bottom-0 right-0 p-2 text-xs">
			© {new Date().getFullYear()} - presented by{' '}
			<a
				href="https://siblanco.dev"
				onClick={(e) => openLinkInDefaultBrowser(e)}
				className="text-teal-700"
			>
				www.siblanco.dev
			</a>
		</div>
	</div>
);

export default Layout;
