import React from 'react';
import { NavLink } from 'react-router-dom';
import { IoIosPerson, IoIosCalculator, IoIosInformationCircle, IoIosOptions } from 'react-icons/io';

type NavPoint = {
	name: string;
	path: string;
	icon: JSX.Element;
};

const Navigation = () => {
	const navPoints: NavPoint[] = [
		{
			name: 'Kunden',
			path: '/kunden',
			icon: <IoIosPerson className="w-8 h-8" />,
		},
		{
			name: 'Rechnungen',
			path: '/rechnungen',
			icon: <IoIosCalculator className="w-8 h-8" />,
		},
		{
			name: 'Meine Daten bearbeiten',
			path: '/setup',
			icon: <IoIosOptions className="w-8 h-8" />,
		},
		{
			name: 'Hilfe',
			path: '/help',
			icon: <IoIosInformationCircle className="w-8 h-8" />,
		},
	];

	return (
		<div className="p-4 flex justify-end space-x-6 absolute top-0 right-0">
			{navPoints.map((point) => (
				<NavLink
					key={point.path}
					to={point.path}
					exact={point.path === '/'}
					className="p-2 bg-white text-teal-500 font-display text-center rounded-sm shadow-sm cursor-pointer transition-opacity opacity-50 duration-150 hover:opacity-100"
					activeClassName="opacity-100"
					title={point.name}
				>
					{point.icon}
				</NavLink>
			))}
		</div>
	);
};

export default Navigation;
