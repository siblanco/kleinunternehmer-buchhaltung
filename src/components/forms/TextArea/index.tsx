import React from 'react';

import { useFormContext } from 'react-hook-form';
import Label from './../Label';
import { ITextArea } from '../../../types';

const TextArea = ({ label, name, required, placeholder, value, rows = 10 }: ITextArea) => {
	const { register, errors } = useFormContext();

	return (
		<>
			<Label label={label} />
			<textarea
				rows={rows}
				className={`appearance-none w-full text-gray-900 border border-transparent bg-white focus:border-teal-900 transition-colors duration-150 px-4 py-2 rounded shadow-sm leading-tight focus:outline-none inline-block ${
					errors[name] && 'border-red-400'
				}`}
				name={name}
				ref={register({
					required,
				})}
				defaultValue={value}
				placeholder={placeholder}
			/>
		</>
	);
};

export default TextArea;
