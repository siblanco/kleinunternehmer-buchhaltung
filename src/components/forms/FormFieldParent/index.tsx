import React from 'react';

export interface IFormFieldParentProps {
	width?: boolean;
	children: React.ReactNode;
}

const FormFieldParent = ({ children, width = false }: IFormFieldParentProps) => (
	<div className={`flex-grow my-3 pr-6 ${width ? width : 'w-full'}`}>{children}</div>
);

export default FormFieldParent;
