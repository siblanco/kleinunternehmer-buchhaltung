import React, { useEffect } from 'react';
import { useForm, SubmitHandler, FormProvider } from 'react-hook-form';

import FormFieldParent from './../FormFieldParent';
import Button from '../../common/Button';
import FormError from '../FormError';

export interface IFormProps {
	onSubmit: SubmitHandler<any>;
	submitText?: string;
	submitSuccess: boolean;
	successAction?: () => void;
	successMsg: string;
	errorMsg: string;
	children: JSX.Element[];
}

const Form = ({
	onSubmit,
	submitText = 'Speichern',
	submitSuccess,
	successAction,
	successMsg,
	errorMsg,
	children,
}: IFormProps) => {
	const formMethods = useForm({
		mode: 'onChange',
	});

	useEffect(() => {
		if (submitSuccess) {
			formMethods.reset();
			if (successAction) {
				successAction();
			}
		}
	}, [submitSuccess]);

	return (
		<FormProvider {...formMethods}>
			<form className="flex flex-wrap relative">
				{children.length > 1 ? (
					children.map((child) => (
						<FormFieldParent
							key={child.props.name ?? child.props['data-name']}
							width={child.props.width ?? child.props['data-width']}
						>
							{child}
							<FormError error={formMethods.errors[child.props.name]} />
						</FormFieldParent>
					))
				) : (
					<h2>Bitte mindestens 2 Formelemente einfügen.</h2>
				)}

				{submitSuccess && (
					<div className="mt-8 w-full text-gray-700 font-display">{successMsg}</div>
				)}

				{errorMsg.length > 0 && (
					<div className="mt-8 w-full text-red-500 font-display">{errorMsg}</div>
				)}

				<div className="mt-6 w-full">
					<Button
						type="button"
						disabled={!formMethods.formState.isDirty || submitSuccess}
						handleClick={formMethods.handleSubmit(onSubmit)}
					>
						{submitText}
					</Button>
				</div>
			</form>
		</FormProvider>
	);
};

export default Form;
