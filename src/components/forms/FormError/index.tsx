import React from 'react';
import { FieldError } from 'react-hook-form';

const FormError = ({ error }: { error: FieldError }) => (
	<p className="mt-1 text-red-500 uppercase font-display text-xs">
		{error && error.type === 'required' && (error.message ? error.message : 'Pflichtfeld')}
		{error && error.type === 'pattern' && (error.message ? error.message : 'Pflichtfeld')}
	</p>
);

export default FormError;
