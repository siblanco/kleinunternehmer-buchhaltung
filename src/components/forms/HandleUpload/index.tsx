const fs = window.require('fs');
const { remote } = window.require('electron');
const { basename } = window.require('path');
const mime = window.require('mime-types');

import React, { useState, useEffect } from 'react';
import { useFormContext } from 'react-hook-form';
import Button from '../../common/Button';
import Label from '../Label';

interface IHandleUploadProps {
	formSent: boolean;
	label: string;
	value: string;
	extensions: string[];
	type?: string;
}

const HandleUpload = ({
	formSent,
	label,
	value,
	extensions,
	type = 'path',
}: IHandleUploadProps) => {
	const [filePath, setFilePath] = useState('');
	const [fileType, setFileType] = useState('');
	const { setValue } = useFormContext();

	useEffect(() => {
		if (formSent) setFilePath('');
	}, [formSent]);

	const uploadFile = async () => {
		try {
			const files = await remote.dialog.showOpenDialog({
				properties: ['openFile'],
				filters: [{ name: label, extensions }],
			});
			let uploadedFilepath = files.filePaths[0];

			if (type !== 'path') {
				const fileType = mime.lookup(uploadedFilepath);
				uploadedFilepath = fs.readFileSync(uploadedFilepath, 'base64');

				setValue(value, `data:${fileType};base64,${uploadedFilepath}`, {
					shouldDirty: true,
				});

				setFileType(fileType);
			} else {
				setValue(value, uploadedFilepath, { shouldDirty: true });
			}

			setFilePath(uploadedFilepath);
		} catch (e) {
			console.log(e);
		}
	};

	if (type !== 'path') {
		return (
			<>
				{filePath && (
					<div className="w-full mb-4">
						<Label label="Neues Logo" />
						<img
							className="w-64"
							src={`data:${fileType};base64, ${filePath}`}
							alt="Logo"
						/>
					</div>
				)}

				<Button handleClick={() => uploadFile()}>{label}</Button>
			</>
		);
	}

	return (
		<div className="flex items-center flex-wrap">
			<Button handleClick={() => uploadFile()}>{label}</Button>
			{filePath && <p className="ml-4 font-display">{basename(filePath)}</p>}
		</div>
	);
};

export default HandleUpload;
