import React from 'react';

interface ILabel {
	label: string;
	hidden?: boolean;
}

const Label = ({ label, hidden = false }: ILabel) => {
	return (
		<label
			className={`font-display text-xs uppercase mb-1 inline-block ${hidden ? 'hidden' : ''}`}
		>
			{label}
		</label>
	);
};

export default Label;
