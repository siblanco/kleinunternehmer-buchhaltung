import React from 'react';

import { useFormContext } from 'react-hook-form';
import Label from '../Label';
import { ISelect } from '../../../types';

const Select = ({ label, name, options, required, value }: ISelect) => {
	const { register } = useFormContext();

	return (
		<div className="relative">
			<Label label={label} />
			<select
				className="block appearance-none w-full bg-white border border-transparent focus:border-teal-900 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none"
				name={name}
				ref={register({
					required,
				})}
				defaultValue={value}
			>
				{options.map((option) => (
					<option key={option.value} value={option.value}>
						{option.name}
					</option>
				))}
			</select>
		</div>
	);
};

export default Select;
