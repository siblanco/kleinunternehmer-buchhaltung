import React from 'react';
import Select from '../Select';
import { TCurrencies } from '../../../types';

export const currencySelectOptions: TCurrencies[] = ['euro', 'chf'];

export const CurrencySelect = ({ width }: { width: string }) => (
	<Select
		required
		width={width}
		label="Währung"
		name="currency"
		options={currencySelectOptions.map((cur) => ({
			name: cur.toUpperCase(),
			value: cur,
		}))}
	/>
);
