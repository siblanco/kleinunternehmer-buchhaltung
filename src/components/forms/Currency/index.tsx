import React, { useState, useEffect } from 'react';
import * as currencyFormatter from '../../../node/currency';

import Input from '../Input';
import { useFormContext } from 'react-hook-form';

interface ICurrencyProps {
	name: string;
	label: string;
	reset: boolean;
}

const CurrencyLabel = ({ name, label, reset }: ICurrencyProps) => {
	const [preview, setPreview] = useState('');
	const { watch } = useFormContext();

	const currency = watch('currency', 'euro');

	useEffect(() => {
		if (reset) {
			setPreview('');
		}
	}, [reset]);

	const formatAndPreview = (value: string) => {
		const price = (currencyFormatter as any)[currency](value).format();
		setPreview(price);
	};

	return (
		<div>
			<Input name={name} extraHandler={formatAndPreview} label={label} required />
			<div className="text-xs font-display pl-4 pt-1">{preview}</div>
		</div>
	);
};

export default CurrencyLabel;
