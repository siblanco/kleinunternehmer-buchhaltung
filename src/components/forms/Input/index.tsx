import React from 'react';
import { useFormContext } from 'react-hook-form';

import { IFormField } from '../../../types';
import Label from '../Label';

const Input = ({
	label,
	name,
	required = false,
	value,
	readOnly = false,
	checked = false,
	disabled = false,
	hidden = false,
	type = 'text',
	extraHandler,
}: IFormField) => {
	const { register, errors } = useFormContext();

	const emailPattern = {
		value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
		message: 'Ungültige E-Mail-Adresse',
	};

	return (
		<div>
			<Label label={label} hidden={hidden} key={value} />
			<input
				type={type}
				className={`input-field ${errors[name] ? 'border-red-500' : ''} ${
					disabled ? 'cursor-not-allowed opacity-50' : ''
				} ${hidden ? 'hidden' : ''}`}
				defaultValue={value}
				name={name}
				checked={checked ? true : undefined}
				disabled={disabled}
				readOnly={readOnly}
				ref={register({
					required,
					...(name === 'email' && { pattern: emailPattern }),
				})}
				{...(type === 'date' && { pattern: 'd{4}-d{2}-d{2}' })}
				{...(extraHandler && { onChange: (e) => extraHandler(e.target.value) })}
			/>
		</div>
	);
};

export default Input;
