import React from 'react';
import { useFormContext } from 'react-hook-form';
import { IFormField } from '../../../types';

const Checkbox = ({ name, label, required, disabled = false }: IFormField) => {
	const { register } = useFormContext();

	return (
		<div className={`${disabled ? 'opacity-50' : ''}`}>
			<label className={`block font-display ${disabled ? 'pointer-events-none' : ''}`}>
				<input
					className="mr-2 leading-tight"
					type="checkbox"
					key={name}
					name={name}
					defaultChecked={disabled}
					disabled={disabled}
					ref={register({
						required,
					})}
				/>
				<span className="font-normal text-antrok-green-500">{label}</span>
			</label>
		</div>
	);
};

export default Checkbox;
