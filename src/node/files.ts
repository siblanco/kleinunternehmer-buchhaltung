const os = window.require('os');
const fs = window.require('fs');

interface IFilesProps {
	homedir: string;
	invoicePath: string;
	months: string[];

	init: () => void;
	getHomePath: () => string;
	createFolder: (path: string) => void;
	createInvoicePath: (date: string, type: string) => void;
}

export const Files: IFilesProps = {
	homedir: '',
	invoicePath: '',
	months: [
		'Januar',
		'Februar',
		'März',
		'April',
		'Mai',
		'Juni',
		'Juli',
		'August',
		'September',
		'Oktober',
		'November',
		'Dezember',
	],

	init() {
		const home = os.homedir();
		this.createFolder(`${home}/Buchhaltung`);
		this.homedir = `${home}/Buchhaltung`;
	},

	getHomePath() {
		return `${this.homedir}`;
	},

	createFolder(path) {
		if (!fs.existsSync(path)) {
			fs.mkdirSync(path);
		}
	},

	createInvoicePath(date, type) {
		// test date
		const datepatt = new RegExp('^([0-9]{4})-([0-9]{2})-([0-9]{2})$');
		if (
			!datepatt.test(date) ||
			Number(date.substr(5, 2)) > 12 ||
			Number(date.substr(8, 2)) > 31
		) {
			throw new Error('Not a valid date.');
		}

		const year = date.substr(0, 4);
		this.createFolder(`${this.homedir}/${year}`);

		const month = this.months[Number(date.substr(5, 2)) - 1];
		this.createFolder(`${this.homedir}/${year}/${month}`);

		if (type == 'income') {
			this.createFolder(`${this.homedir}/${year}/${month}/Einnahmen`);
			this.invoicePath = `${this.homedir}/${year}/${month}/Einnahmen`;
		} else {
			this.createFolder(`${this.homedir}/${year}/${month}/Ausgaben`);
			this.invoicePath = `${this.homedir}/${year}/${month}/Ausgaben`;
		}

		return this.invoicePath;
	},
};

Files.init();
