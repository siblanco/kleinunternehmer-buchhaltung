import { db } from './db';
import {
	IRechnungsPosition,
	IUser,
	ICustomer,
	IInvoice,
	ECurrencySign,
	ECurrencyTax,
} from '../types';

const fonts = {
	Helvetica: {
		normal: 'Helvetica',
		bold: 'Helvetica-Bold',
		italics: 'Helvetica-Oblique',
		bolditalics: 'Helvetica-BoldOblique',
	},
	Symbol: {
		normal: 'Symbol',
	},
	ZapfDingbats: {
		normal: 'ZapfDingbats',
	},
};

const PdfPrinter = window.require('pdfmake');
export const printer = new PdfPrinter(fonts);

export function generateDocDefinition(invoicen: string) {
	const user: IUser = db.get('user').value();
	const invoice: IInvoice = db.get('invoices').find({ invoicen }).value();
	const customer: ICustomer = db.get('customers').find({ customern: invoice.customern }).value();

	const positions: any[] = [];
	const invoicePositions = invoice.positions as unknown as IRechnungsPosition[];

	invoicePositions.forEach((pos) => {
		positions.push([
			{
				text: pos.desc,
				border: [false, false, false, true],
				margin: [0, 5, 0, 5],
				alignment: 'left',
			},
			{
				border: [false, false, false, true],
				text: pos.price,
				fillColor: '#f5f5f5',
				alignment: 'right',
				margin: [0, 5, 0, 5],
			},
		]);
	});

	return {
		content: [
			{
				columns: [
					{
						image: user.logo,
						width: 125,
					},
					[
						{
							text: 'Rechnung',
							color: '#333333',
							width: '*',
							fontSize: 28,
							bold: true,
							alignment: 'right',
							margin: [0, 0, 0, 15],
						},
						{
							stack: [
								{
									columns: [
										{
											text: 'Rechnungsnummer',
											color: '#aaaaab',
											bold: true,
											width: '*',
											fontSize: 12,
											alignment: 'right',
										},
										{
											text: invoicen,
											bold: true,
											color: '#333333',
											fontSize: 12,
											alignment: 'right',
											width: 100,
										},
									],
								},
								{
									columns: [
										{
											text: 'Rechnungsdatum',
											color: '#aaaaab',
											bold: true,
											width: '*',
											fontSize: 12,
											alignment: 'right',
										},
										{
											text: invoice.date,
											bold: true,
											color: '#333333',
											fontSize: 12,
											alignment: 'right',
											width: 100,
										},
									],
								},
								{
									columns: [
										{
											text: 'Kundennummer',
											color: '#aaaaab',
											bold: true,
											fontSize: 12,
											alignment: 'right',
											width: '*',
										},
										{
											text: invoice.customern,
											bold: true,
											fontSize: 12,
											alignment: 'right',
											width: 100,
										},
									],
								},
								customer.uid && customer.uid.length
									? {
											columns: [
												{
													text: 'UID des Empfängers',
													color: '#aaaaab',
													bold: true,
													fontSize: 12,
													alignment: 'right',
													width: '*',
												},
												{
													text: `${customer.uid}`,
													bold: true,
													fontSize: 12,
													alignment: 'right',
													width: 100,
												},
											],
									  }
									: {},
								{
									columns: [
										{
											text: 'Zahlungsziel',
											color: '#aaaaab',
											bold: true,
											fontSize: 12,
											alignment: 'right',
											width: '*',
										},
										{
											text: `${invoice.termOfPayment} Tage`,
											bold: true,
											fontSize: 12,
											alignment: 'right',
											width: 100,
										},
									],
								},
							],
						},
					],
				],
			},
			{
				columns: [
					{
						text: 'Von',
						color: '#aaaaab',
						bold: true,
						fontSize: 14,
						alignment: 'left',
						margin: [0, 20, 0, 5],
					},
					{
						text: 'An',
						color: '#aaaaab',
						bold: true,
						fontSize: 14,
						alignment: 'left',
						margin: [0, 20, 0, 5],
					},
				],
			},
			{
				columns: [
					{
						text: `${user.vorname} ${user.nachname} \n ${user.company}`,
						bold: true,
						color: '#333333',
						alignment: 'left',
					},
					{
						text: `${customer.name !== '-' ? customer.name + '\n' : ''}${
							customer.company
						}`,
						bold: true,
						color: '#333333',
						alignment: 'left',
					},
				],
			},
			{
				columns: [
					{
						text: 'Adresse',
						color: '#aaaaab',
						bold: true,
						margin: [0, 7, 0, 3],
					},
					{
						text: 'Adresse',
						color: '#aaaaab',
						bold: true,
						margin: [0, 7, 0, 3],
					},
				],
			},
			{
				columns: [
					{
						text: `${user.street} \n ${user.plz} ${user.city} \n Deutschland`,
						style: 'invoiceBillingAddress',
					},
					{
						text: `${customer.address} \n ${customer.postalcode} ${customer.city}${
							customer.country ? '\n' + customer.country : ''
						}`,
						style: 'invoiceBillingAddress',
					},
				],
			},
			'\n\n',
			{
				width: '100%',
				alignment: 'center',
				text: `Rechnungsnummer ${invoicen}`,
				bold: true,
				margin: [0, 10, 0, 10],
				fontSize: 15,
			},
			{
				layout: {
					defaultBorder: false,
					hLineWidth: function () {
						return 1;
					},
					vLineWidth: function () {
						return 1;
					},
					hLineColor: function (i: number) {
						if (i === 1 || i === 0) {
							return '#bfdde8';
						}
						return '#eaeaea';
					},
					vLineColor: function () {
						return '#eaeaea';
					},
					hLineStyle: function () {
						return null;
					},
					paddingLeft: function () {
						return 10;
					},
					paddingRight: function () {
						return 10;
					},
					paddingTop: function () {
						return 2;
					},
					paddingBottom: function () {
						return 2;
					},
					fillColor: function () {
						return '#fff';
					},
				},
				table: {
					headerRows: 1,
					widths: ['*', 80],
					body: [
						[
							{
								text: 'BEZEICHNUNG',
								fillColor: '#eaf2f5',
								border: [false, true, false, true],
								margin: [0, 5, 0, 5],
								textTransform: 'uppercase',
							},
							{
								text: 'PREIS',
								border: [false, true, false, true],
								alignment: 'right',
								fillColor: '#eaf2f5',
								margin: [0, 5, 0, 5],
								textTransform: 'uppercase',
							},
						],
						...positions,
					],
				},
			},
			'\n',
			'\n\n',
			{
				layout: {
					defaultBorder: false,
					hLineWidth: function () {
						return 1;
					},
					vLineWidth: function () {
						return 1;
					},
					hLineColor: function () {
						return '#eaeaea';
					},
					vLineColor: function () {
						return '#eaeaea';
					},
					hLineStyle: function () {
						return null;
					},
					paddingLeft: function () {
						return 10;
					},
					paddingRight: function () {
						return 10;
					},
					paddingTop: function () {
						return 3;
					},
					paddingBottom: function () {
						return 3;
					},
					fillColor: function () {
						return '#fff';
					},
				},
				table: {
					headerRows: 1,
					widths: ['*', 'auto'],
					body: [
						[
							{
								text: 'Nettobetrag',
								border: [false, true, false, true],
								alignment: 'right',
								margin: [0, 5, 0, 5],
							},
							{
								border: [false, true, false, true],
								text: invoice.total,
								alignment: 'right',
								fillColor: '#f5f5f5',
								margin: [0, 5, 0, 5],
							},
						],
						[
							{
								text: 'Umsatzsteuer',
								border: [false, false, false, true],
								alignment: 'right',
								margin: [0, 5, 0, 5],
							},
							{
								text: ` ${
									ECurrencySign[
										invoice.currency.toUpperCase() as keyof typeof ECurrencySign
									]
								} 0.00`,
								border: [false, false, false, true],
								fillColor: '#f5f5f5',
								alignment: 'right',
								margin: [0, 5, 0, 5],
							},
						],
						[
							{
								text: 'Rechnungsbetrag',
								bold: true,
								fontSize: 20,
								alignment: 'right',
								border: [false, false, false, true],
								margin: [0, 5, 0, 5],
							},
							{
								text: invoice.total,
								bold: true,
								fontSize: 20,
								alignment: 'right',
								border: [false, false, false, true],
								fillColor: '#f5f5f5',
								margin: [0, 5, 0, 5],
							},
						],
					],
				},
			},
			'\n\n',
			{
				text: invoice.performancePeriod,
				style: 'notesTitle',
			},
			{
				text: `${
					ECurrencyTax[invoice.currency.toUpperCase() as keyof typeof ECurrencyTax]
				}`,
				style: 'notesText',
			},
			{
				columns: [
					{
						text: `SteuerNr.: ${user.taxno}`,
						style: 'notesText',
						margin: [0, 20, 0, 0],
					},
					{
						text: `Tel ${user.tel}\nEmail ${user.email}\n${user.web}`,
						style: 'notesText',
						margin: [0, 20, 0, 0],
					},
					{
						text: `Bankverbindung\nIBAN ${user.iban}\nBIC ${user.bic}`,
						style: 'notesText',
						margin: [0, 20, 0, 0],
					},
				],
			},
		],
		styles: {
			notesTitle: {
				fontSize: 10,
				bold: true,
				margin: [0, 10, 0, 3],
			},
			notesText: {
				fontSize: 10,
			},
		},
		defaultStyle: {
			columnGap: 20,
			font: 'Helvetica',
			lineHeight: 1.5,
		},
	};
}
