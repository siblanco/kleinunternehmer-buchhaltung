const excel = window.require('exceljs');
const fs = window.require('fs');
const path = window.require('path');

import { Files } from './files';
import { db } from './db';
import { IExpense } from '../types';
import { euro, chf } from './currency';

interface IExcelOpProps {
	wb: any;
	ws: any;
	filepath: string;

	init: () => Promise<any>;
	openEUR: (filepath: string) => any;

	findNextRow: (type: 'income' | 'expense') => any;
	addNewIncome: (invoicen: string) => any;
	addNewExpense: (expense: IExpense) => any;
	updateTotals: () => void;
}

export const ExcelOp: IExcelOpProps = {
	wb: null,
	ws: null,
	filepath: '',

	init() {
		const year = new Date().getFullYear();
		const root = window.require('electron').remote.app.getAppPath();

		Files.createFolder(Files.getHomePath() + '/EUR');
		Files.createFolder(`${Files.getHomePath()}/EUR/${year}`);
		const EURpath = `${Files.getHomePath()}/EUR/${year}/EUR.xlsx`;

		if (!fs.existsSync(EURpath)) {
			fs.copyFileSync(path.join(root, 'EUR.xlsx'), EURpath);
		}

		return this.openEUR(EURpath);
	},

	async openEUR(filepath) {
		this.wb = await new excel.Workbook().xlsx.readFile(filepath);
		this.ws = this.wb.getWorksheet(1);
		this.filepath = filepath;
	},

	findNextRow(type) {
		let rowToWrite;
		let existingRow = false;
		// start from 2 because 1 is headlines
		for (let rowNumber = 2; rowNumber <= this.ws.rowCount; rowNumber++) {
			const row = this.ws.getRow(rowNumber);

			if (type === 'income') {
				const posten = row.getCell('A').value;
				if (!posten) {
					rowToWrite = row;
					existingRow = true;
					break;
				}
			} else if (type === 'expense') {
				const posten = row.getCell('E').value;
				if (!posten) {
					rowToWrite = row;
					existingRow = true;
					break;
				}
			}
		}

		if (!existingRow) {
			this.ws.actualRowCount === 1
				? (rowToWrite = this.ws.getRow(2))
				: (rowToWrite = this.ws.getRow(this.ws.rowCount + 1));
		}

		return rowToWrite;
	},

	addNewIncome(invoicen) {
		const invoice = db.get('invoices').find({ invoicen }).value();

		const row = this.findNextRow('income');

		const positions = [];
		for (const pos of invoice.positions) {
			positions.push(pos.desc);
		}

		row.getCell('A').value = positions.join(' | ');
		row.getCell('B').value =
			invoice.currency === 'chf' ? chf(invoice.total).value : euro(invoice.total).value;

		row.getCell('C').value = invoice.invoicen;
		row.getCell('D').value = invoice.date;
		row.commit();

		this.updateTotals();

		return this.wb.xlsx.writeFile(this.filepath);
	},

	addNewExpense(expense) {
		const row = this.findNextRow('expense');
		row.getCell('E').value = expense.desc;
		row.getCell('F').value = euro(expense.price).value;
		row.getCell('G').value = expense.invoicen;
		row.getCell('H').value = expense.date;
		row.commit();

		this.updateTotals();

		return this.wb.xlsx.writeFile(this.filepath);
	},

	updateTotals() {
		const totalIncomeRow = this.ws.getRow(4);
		const totalExpensesRow = this.ws.getRow(5);
		const totalWin = this.ws.getRow(7);

		totalIncomeRow.getCell('K').value = { formula: '=SUM(B2:B1500)' };
		totalExpensesRow.getCell('K').value = { formula: '=SUM(F2:F100)' };
		totalWin.getCell('K').value = { formula: '=K4-K5' };

		totalIncomeRow.commit();
		totalExpensesRow.commit();
		totalWin.commit();
	},
};

ExcelOp.init();
