const low = window.require('lowdb');
const FileSync = window.require('lowdb/adapters/FileSync');
import { Files } from './files';

// create DB Folder
Files.createFolder(Files.getHomePath() + '/DB');

const adapter = new FileSync(`${Files.getHomePath()}/DB/db.json`);
const db = low(adapter);

db.defaults({
	user: {},
	invoices: [],
	customers: [],
	expenses: [],
}).write();

export { db };
