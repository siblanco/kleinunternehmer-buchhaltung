import { db } from './db';
import { IUser } from '../types';

interface IUserProps {
	updateUserData: (data: IUser) => boolean;
	getUserData: () => IUser;
}

export const User: IUserProps = {
	getUserData() {
		return db.get('user').value();
	},

	updateUserData(updatedUserData) {
		const user = this.getUserData();

		if (!updatedUserData.logo) {
			updatedUserData.logo = user.logo;
		}

		db.get('user').assign(updatedUserData).write();
		return true;
	},
};
