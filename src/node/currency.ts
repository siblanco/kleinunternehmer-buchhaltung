const currency = window.require('currency.js');

export const euro = (value: number | string) =>
	currency(value, {
		symbol: '€ ',
		separator: '.',
		decimal: ',',
	});

export const chf = (value: number | string) =>
	currency(value, {
		symbol: 'CHF ',
		separator: '.',
		decimal: ',',
	});
