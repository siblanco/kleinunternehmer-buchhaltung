const archiver = window.require('archiver');
const fs = window.require('fs');

export const zipDirectory = (source: string, out: string) => {
	const archive = archiver('zip', { zlib: { level: 9 } });
	const stream = fs.createWriteStream(out);

	return new Promise((resolve, reject) => {
		archive
			.directory(source, false)
			.on('error', (err: any) => reject(err))
			.pipe(stream);

		stream.on('close', () => resolve(true));
		archive.finalize();
	});
};
