import { db } from './db';
import { ICustomer } from '../types';

interface ICustomersProps {
	createCustomer: (customer: ICustomer) => boolean;
	getCustomer: (customern: string) => ICustomer;
	getAllCustomers: () => ICustomer[];
	editCustomer: (updatedCustomer: ICustomer) => boolean;
	deleteCustomer: (customern: string) => void;
}

export const Customers: ICustomersProps = {
	createCustomer(customer: ICustomer) {
		const allCustomers = Customers.getAllCustomers();
		const customernExists = allCustomers.filter((existingCustomer) => {
			return existingCustomer.customern === customer.customern;
		}).length;

		if (customernExists) {
			return false;
		}

		const user: ICustomer = {
			name: customer.name,
			company: customer.company,
			address: customer.address,
			city: customer.city,
			postalcode: customer.postalcode,
			customern: customer.customern,
			uid: customer.uid,
			country: customer.country,
		};

		db.get('customers').push(user).write();
		return true;
	},

	editCustomer(data) {
		db.get('customers')
			.find({ customern: data.customern })
			.assign({ ...data })
			.write();

		return true;
	},

	getCustomer(customern) {
		return db.get('customers').find({ customern }).value();
	},

	getAllCustomers() {
		return db.get('customers').value();
	},

	deleteCustomer(customern: string) {
		db.get('customers').remove({ customern }).write();
	},
};
