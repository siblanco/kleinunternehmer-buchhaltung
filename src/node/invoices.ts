const fs = window.require('fs');
const os = window.require('os');

const { join, basename } = window.require('path');

import { db } from './db';
import { ExcelOp } from './excelop';
import { Files } from './files';
import * as currencyFormatter from './currency';
import { printer, generateDocDefinition } from './pdfmake';
import { generateGermanDate } from '../utils';
import { IInvoice, IRechnungsPosition, IExpense } from '../types';

interface IInvoicesProps {
	getInvoice: (invoicen: string) => IInvoice;
	getAllInvoices: () => IInvoice[];
	getNextInvoiceNumber: () => string;
	createInvoice: (invoice: IInvoice) => Promise<any> | boolean;
	rewriteExistingInvoice: (invoicen: string) => Promise<boolean>;
	createExpense: (expense: IExpense) => Promise<any> | boolean;
}

export const Invoices: IInvoicesProps = {
	getInvoice(invoicen) {
		return db.get('invoices').find({ invoicen }).value();
	},
	getAllInvoices() {
		return db.get('invoices').value();
	},
	getNextInvoiceNumber() {
		const currentYear = new Date().getFullYear().toString();
		const currentMonth = (new Date().getMonth() + 1).toString().padStart(2, '0');
		const [lastInvoice] = db.get('invoices').takeRight(1).value();
		const invoiceType = db.get('user').value().invoiceType ?? 'yearly';

		switch (invoiceType) {
			case 'monthly': {
				if (
					!lastInvoice ||
					currentYear !== lastInvoice.invoicen.substr(0, 4) ||
					lastInvoice.invoicen.substr(5, 2) !== currentMonth
				) {
					return `${currentYear}-${currentMonth}-0001`;
				}

				return `${currentYear}-${currentMonth}-${(
					Number(lastInvoice.invoicen.substr(8)) + 1
				)
					.toString()
					.padStart(4, '0')}`;
			}
			case 'yearly':
			default: {
				if (!lastInvoice || lastInvoice.invoicen.substr(0, 4) !== currentYear) {
					return `${currentYear}-0001`;
				}

				return `${currentYear}-${(Number(lastInvoice.invoicen.substr(5)) + 1)
					.toString()
					.padStart(4, '0')}`;
			}
		}
	},

	async createInvoice(invoice) {
		const nextInvoiceNumber = Invoices.getNextInvoiceNumber();

		try {
			const positions = JSON.parse(invoice.positions);
			const total = positions
				.map((pos: IRechnungsPosition) => {
					return currencyFormatter[invoice.currency](pos.price);
				})
				.reduce((a: any, b: any) => a.add(b.value), currencyFormatter[invoice.currency](0));

			const newInvoice: IInvoice = {
				invoicen: nextInvoiceNumber,
				customern: invoice.customern,
				positions: positions,
				termOfPayment: invoice.termOfPayment,
				total: total.format(),
				date: generateGermanDate(invoice.date),
				currency: invoice.currency,
				performancePeriod: invoice.performancePeriod,
			};

			db.get('invoices').push(newInvoice).write();

			const docDef = generateDocDefinition(nextInvoiceNumber);
			const pdfDoc = printer.createPdfKitDocument(docDef);
			await pdfDoc.pipe(
				fs.createWriteStream(
					Files.createInvoicePath(invoice.date, 'income') +
						'/' +
						nextInvoiceNumber +
						'.pdf'
				)
			);
			pdfDoc.end();
			// // füge zu excel hinzu
			ExcelOp.addNewIncome(nextInvoiceNumber);
			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	},

	async rewriteExistingInvoice(invoicen) {
		try {
			const docDef = generateDocDefinition(invoicen);
			const pdfDoc = printer.createPdfKitDocument(docDef);

			await pdfDoc.pipe(fs.createWriteStream(`${os.homedir()}/Buchhaltung/${invoicen}.pdf`));

			pdfDoc.end();

			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	},

	createExpense(expense) {
		try {
			const expenseFilePath = Files.createInvoicePath(expense.date, 'expense');
			const germanDate = generateGermanDate(expense.date);
			expense.date = germanDate;

			db.get('expenses').push(expense).write();
			ExcelOp.addNewExpense(expense);

			// move pdf to right folders by date
			fs.copyFileSync(expense.pdf, join(expenseFilePath, basename(expense.pdf)));
			// fs.unlinkSync(expense.pdf);

			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	},
};
