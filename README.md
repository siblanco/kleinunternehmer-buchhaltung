# Buchhaltungsapp für Kleinunternehmer

# Development

To start the development app locally, run:

    $ npm start
    # then in a different terminal
    $ npm run electron

# Build for Production

Build with electron-builder for your os

    $ npm run build

# Ideas / todos

-   Offer different layouts for invoices
-   Customer No. and Invoice No. changeable
-   ~~Ausgaben zeigen~~
-   ~~Komma Zahlen zulassen~~
-   ~~Edit User Bild anzeigen~~
